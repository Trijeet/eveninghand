class Monitor:
	"""Monitor is a system that keeps track of employees and their to-do tasks."""
	def __init__(self, task_list: list, employees: list) -> None:
		##Save the values for the object
		self.task_list = task_list
		self.employees = employees

		##Manage current and completed tasks as employee, task_list pairs
		self.scheduler = {}
		self.complete_tasks = {}

		##For each employee, make a to-do and a completed list
		###Task list
		for employee in employees:
			self.scheduler[employee] = []
		###To-Do list
		for employee in employees:
			self.complete_tasks[employee] = []

	def add_a_task(self, employee_name: str, new_task: str) -> None:
		self.scheduler[employee_name].append(new_task)

	def assign_tasks(self) -> None:
		tasker = {}
		counter = 0
		for employee in self.employees:
			tasker[counter] = employee
			counter += 1

		counter = 0
		for task in self.task_list:
			##Make sure that the tasks get split up into 4 equal-ish groups
			this_assignee = counter % len(self.employees)
			counter += 1

			##Get the corresponding employee
			this_employee = tasker[this_assignee]
			##The scheduler has employee, task_list pairs.
			self.scheduler[this_employee].append(task)
		return

	def check_employee(self, employee_name: str) -> None:
		print(self.scheduler[employee_name])
		return

	def complete_task(self, employee_name: str, task_name: str) -> None:
		self.scheduler[employee_name].remove(task_name)
		self.complete_tasks[employee_name].append(task_name)
		return

	def how_many_remaining(self, employee_name: str) -> int:
		return len(self.scheduler[employee_name])

	def show_completed(self, employee_name: str) -> None:
		print(self.complete_tasks[employee_name])

		