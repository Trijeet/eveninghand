import monitoring_system as ms

employees = ["Person A", "Person B", "Person C", "Person D"]

task_list = ["complete 1", "complete 2", "complete 3", "complete 5", "complete 6", "complete 7", "complete 8", "complete 9", "complete 10"]

app = ms.Monitor(task_list, employees)
app.assign_tasks()
print(app.scheduler)

app.check_employee("Person A")

app.complete_task(employee_name = "Person A", task_name = "complete 1")

app.show_completed("Person A")

app.add_a_task("Person A", "new_task_sad")

app.check_employee("Person A")