#!/usr/bin/env python3

##Apps
from flask import Flask
from flask_restful import Resource, Api, reqparse, abort
import os
import random

class RandomNumberGenerator(object):
    def __init__(self):
        self.arg = "Hello"

    def doRandom(self, n):
        return n*100

class RandomService(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        args = parser.parse_args()

        return (rngesus.doRandom(5)), 200

if __name__ == '__main__':

    rngesus = RandomNumberGenerator()

    app = Flask(__name__)
    api = Api(app)

    api.add_resource(RandomService, '/')
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))