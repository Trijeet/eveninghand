#!/usr/bin/env python3

##Apps
from flask import Flask
from flask_restful import Resource, Api, reqparse, abort
# import pandas as pd
import os
import random

def service1(req):
    print(req)
    return random.random()

def check_user(user_id):
    if user_id not in DATA["users"]:
        abort(404, message=f"User {user_id} doesn't exist")

class NumberList(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('user_id', required=True)
        parser.add_argument('payload', required=True)
        args = parser.parse_args()
        
        if args["user_id"] not in DATA["users"].keys():
            ##Make the user and put the number in.
            DATA["users"][args["user_id"]] = {"numbers": [args["payload"]],}
            return DATA["users"][args["user_id"]], 200
        else:
            return {"message": f'User {args["user_id"]} already exists.'}, 401
    
    # def get(self, user_id):
    #     check_user(user_id)
    #     # return DATA["users"][user_id]["numbers"], 200
    #     return service1(user_id), 200

    def get(self):
        # return DATA["users"][user_id]["numbers"], 200
        parser = reqparse.RequestParser()
        parser.add_argument('user_id', required=True)
        args = parser.parse_args()

        if args["user_id"] not in DATA["users"].keys():
            return {"message": f"User {args['user_id']} doesn't exist."}, 409
        else:
            return (args["user_id"], DATA["users"][args["user_id"]]["numbers"], service1(args["user_id"])), 200
    
    def put(self):
        parser = reqparse.RequestParser()
        parser.add_argument('user_id', required=True)
        parser.add_argument('payload', required=True)
        args = parser.parse_args()
        
        if args["user_id"] not in DATA["users"].keys():
            return {"message": f"User {args['user_id']} doesn't exist."}, 409
        else:
            DATA["users"][args["user_id"]]["numbers"].insert(0,args["payload"])
            return DATA["users"][args["user_id"]]["numbers"], 201

    def delete(self):
        parser = reqparse.RequestParser()
        parser.add_argument('user_id', required=True)
        args = parser.parse_args()

        # check_user(args["user_id"])
        DATA["users"][args["user_id"]]["numbers"] *= 0
        return {"message": "Successfully deleted user data"}, 204

if __name__ == '__main__':
    DATA = {
        "users": {
            "1": {
                "numbers": []
            },
            "2": {
                "numbers": []
            },
            "3": {
                "numbers": []
            },
        },
        "services": [service1],
        "saved_tasks": []
    }
    app = Flask(__name__)
    api = Api(app)

    api.add_resource(NumberList, '/')
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))