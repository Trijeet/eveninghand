from screens import Screen, Screen2
import pytest

class TestScreens(object):

	def test_screen_helper(self):
		correct_helper = Screen()
		assert correct_helper.class_var==5, "You imported the wrong screen."
		return 0

	def test_different_identifier(self):
		correct_helper = Screen2()
		assert correct_helper.class_var==6, "You imported the wrong screen."
		return 0

class TestStuff(object):

	def test_sane(self):
		assert type("")==str

	def test_right_error(self):
		with pytest.raises((TypeError, AttributeError)):
			"help"+5