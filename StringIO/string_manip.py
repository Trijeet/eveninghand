import os

os.mkdir()


# #Before:
# print("My favorite food is mac n cheese.")

# # Formatted String Literals
# my_favorite_food = "Pizza"
# print(f"My favorite food is {my_favorite_food[0:2]}.")

# todays_date = "03/28/2020"


# # Traditional formatting:
# print('The value of pi is approximately %5.4f.' % 3.1415)
# print('The value of pi is approximately %s.' % "woof")

# ##Split, replace
my_best_string = "hello I'm the best, the best, the best"

what_am_i = my_best_string.split(" ")[0]
##Runtime of split(O(n))
###if there's no substring:

# print(type(what_am_i))
# print(what_am_i)


# print(my_best_string.split(" ")[-2:])

print(my_best_string.replace("the", "an"))

"hello".split
tello = "hello".replace("h", "t")

common_verbs = ["had", "ate"]
common_articles = ["the", "an"]
"the dog had a hat" --> split("had") --> replace("the", "") --> subject!