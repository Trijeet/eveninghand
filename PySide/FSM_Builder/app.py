try:
    # Include in try/except block if you're also targeting Mac/Linux
    from PySide2.QtWinExtras import QtWin
    myappid = 'Trijeet.FSMEditor.0.0'
    QtWin.setCurrentProcessExplicitAppUserModelID(myappid)    
except ImportError:
    pass

import sys
from PySide2 import QtWidgets, QtGui
from PySide2.QtUiTools import QUiLoader

loader = QUiLoader()

def load_ui(file_name, where=None):
    """
    Loads a .UI file into the corresponding Qt Python object
    :param file_name: UI file path
    :param where: Use this parameter to load the UI into an existing class (i.e. to override methods)
    :return: loaded UI
    """
    # Create a QtLoader
    loader = QtUiTools.QUiLoader()

    # Open the UI file
    ui_file = QtCore.QFile(file_name)
    ui_file.open(QtCore.QFile.ReadOnly)

    # Load the contents of the file
    ui = loader.load(ui_file, where)

    # Close the file
    ui_file.close()

    return ui

def mainwindow_setup(w):
    w.setWindowTitle("State Machine Builder")

app = QtWidgets.QApplication(sys.argv)

window = loader.load("./mainwindow_test.ui", None)
mainwindow_setup(window)
app.setWindowIcon(QtGui.QIcon('icon.svg'))
window.show()
app.exec_()