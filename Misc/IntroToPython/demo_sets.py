#!/usr/bin/env python3
# encoding: utf-8 

##Define some sets using python:
set1 = {1,2,3}
print(set1)

set2 = set({2,3,3,3,3,3})
print(set2)

#Empty set
empty_set = set()

#But, it's not completely 1:1 with the definitions.
what_am_i = {}
print(type(what_am_i))

##We can evaluate 'truth' using Boolean logic.
#Write an expression, evaluate it's truth table.

#Set membership
fruits = {"apples", "oranges", "tomatoes"}
#"in"
print("apples" in fruits) #truth expression
print("pears" in fruits)

#"Not in"
print("bears" not in fruits)
print("oranges" in fruits)

#Do I have an empty set?
print(not empty_set)
print(not set1)

#What is the cardinality of my set?
print(len(set1))
print(len(empty_set))

#Combining the above two to check if you have an empty set:
print(len(empty_set)==0)