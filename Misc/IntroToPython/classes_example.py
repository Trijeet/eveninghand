#!/usr/bin/env python3
# encoding: utf-8 

class Person(object):
	def __init__(self, n: str, a: int, e: str) -> None:
		self.name = n
		self.age = a
		self.education = v

	def getName(self) -> str:
		return self.name

	def getAge(self) -> int:
		return self.age

	def getEducation(self) -> str:
		return self.education

	def goToWork(self, job_number: int) -> str:
		'''
		This person is a mathematician.
		They work for the ministry of integers. 
		It's their job to find greatest common denominator of a new number every day.
		Their specialty is 360, though.

		Parameters
		----------
		job_number : int

		Returns
		-------
		Greatest common denominator of number and 360

		Raises
		------
		ValueError
			If job_number and 360 GCD takes long to converge

		See Also
		--------
		Trijeet

		Examples
		--------
		>>> person.goToWork(180)
		'''
		a, b = job_number, 360
		safety_counter = 0

		while b > 0:
        	a, b = b, a % b

        	##This comment is for an adjustable safety parameter
        	if safety_counter > 500:
        		raise ValueError
	    return a

	def __str__(self) -> str:
		return f"{self.name} is {self.age} and has a {self.education}."

##This code executes iff this script is called from the command line.
if __name__ == '__main__':
	trijeet = Person("Trijeet", 23, "Bad")
	trijeet.goToWork(180)