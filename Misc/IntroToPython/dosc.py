The Process of a Restaurant:

Objective Function (things that we try to achieve):
1. Good food
2. Good atmosphere
3. Equipment
4. Something unique
5. Cleanliness
6. Good business (profit)
7. Sources
8. Technology (Point-of-sale)
9. Managing 
10.Logging/tracking
11.Good employees
12.Information
13.Theming

Grouping:
1. Good food
	- Sources
	- Equipment
	- Theming

2. Good atmosphere
	- Something unique
	- Theming
	- Cleanliness

3. Managing
	- Logging/tracking
	- Good employees
	- Information
	- Good business (profit)
	- Cleanliness
	- Technology (Point-of-sale)

Ranking:
1. Good food
2. Good management
3. Good atmosphere

Creating:
1. Good food:
	{
		Human
			- Chef
			- Freshness Identifier
		Location
			- Area to store
			- Area to cook
			- Area to serve
		Food
			- Fresh or Ripe
		Money
			- US (fresh)
	}

Quick review of types:
1. String
2. Numeric
3. Truth
4. Nothingness


def identity(name: int) -> int:
	return name

def add(number1: int, number2: int) -> int:
	return number1 + number2

add(10, 10)


class Human(object):
	"""docstring for Human"""
	#constant
	name = "bill"
	#variable
	height = 9
	#functional
	def walk(input):
		happens during
		return output

	def __init__(self, arg):
		super(Human, self).__init__()
		self.arg = arg

class Location(object):
	"""docstring for Location"""
	def __init__(self, arg):
		super(Location, self).__init__()
		self.arg = arg
		
class Food(object):
	"""docstring for Food"""
	def __init__(self, arg):
		super(Food, self).__init__()
		self.arg = arg

class Money(object):
	"""docstring for Money"""
	def __init__(self, arg):
		super(Money, self).__init__()
		self.arg = arg

														