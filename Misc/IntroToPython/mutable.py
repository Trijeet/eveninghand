#Example of mutability in Python
x = ["Item one", 2, "Item 3", 4, 4.5]

#Index a position:
print(x[0]) #returns 'Item one'

#But it's immutable:
x[0] = "New value" #is allowed.

print(x[0]) #will now print 'New value' 