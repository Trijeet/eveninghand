Welcome to the getting started page!

Here I'll outline just a few quick tools that we'll need to get started learning and working together.

Perhaps most importantly, we need to connect! For their suite of tools and for the added value of some anonymity we'll be using a software called Zoom.

First run the installer: https://zoom.us/download
Grab the sign-up link here: https://zoom.us/signup
Then verify your e-mail.


Cooperation is at the heart of what we're about. Working together is a multiplicative effort: twice as many people is truly four times as fast as just the single. We'll be using:
https://gitlab.com

as our collaboration tool.

You may have heard of Github; if you have, Gitlab is the FOSS version of it that doesn't steal your code and information (plus they have a great color scheme).

To get started just follow these steps:

Create an account
Verify the account via e-mail
Set-up an ssh connection*
Create your first repository
Push a readme

1) Just follow the simple sign-up features. Keep the information as minimalist/fake as you like. Pick an easy identifier!
2) I believe in you guys.
3) Setting up an ssh is a little more confusing. If you need help on how to do this, please just reach out to T. If you think you can follow the guide, they have a pretty good one at the top of their page. Go for the RSA option, and use the same e-mail that you did to sign up with your Gitlab account.
https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair
4)
5) 

//Install Zoom as our communication software.

//Get git bash

Register yourself
git config --global user.email "user@domain.com"
git config --global user.name "First Last"

//Then get some lightweight development environment:
SublimeText3
VSCode

//For some basic things we're going to use Python:
https://www.python.org/downloads/

misssparhawk@gmail.com
katherine.wilkins.98@gmail.com
shannonli@shannonli.co
raymond39323@gmail.com
eliomartos@gmail.com