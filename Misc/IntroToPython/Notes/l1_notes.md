//Philosophy
	We learn as a group, speak as a group, cohesive
	Nobody left behind
	Specialize later but foundations are shared

//Using math as a way to understand the world
	The art of problem solving
	Everything is a mathematical object:
	That's a random variable
	That's a function

//How we pick problems to work on
	Fake slide

//how WE pick problems to work on
	Values
	Importance
	For T - development, innovation, and growth outcomes; advancement of science

//How WE answer those problems
	That's up to us.
	I'm not here to lead a group, I'm here to bring us together.

//Tools that we use to answer these problems:
	<b>The toolbox approach</b>
	Computer science:
		programming
		software
	Physics:
		engineering
		theoretical physics
	Mathematics:
		statistics
		information

//Introductions
	It's us!

//First class - show what the expected material to be covered is:
Set theory
```python
object
```
Intro to the command line.

Using Git as a collaboration tool

Python basics

Projects for the group to work on