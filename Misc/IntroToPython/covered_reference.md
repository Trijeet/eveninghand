# Things that we covered:

## Sets
Definition of a set {A}
Arbitrary Elements {x in A}
Logic (True, False)
Relating two elements (or, and, not)
Relating two sets (union, intersection, difference)
A hair on functions

## Programming
Functions (input -> output)
Objects (Group)
Attributes of Objects (Big of Group)
Typing (integer, string, bool)
Namespacing

## Command Line Interface
1. How do I open my command prompt on windows? (Win + R) -> "cmd" -> run
2. How do I make a folder? (mkdir folder_name)
3. How do I make a file? (touch best_file.txt)
4. How do I call a file or program? (software file.ext)
5. How do I know where I am (pwd) (print working directory)
6. How do I know what else is here? (ls) (list)
7. How do I move around? (cd destination) (change directory)

## Python
0. How do I open python? Open your command prompt and type "python"
1. How do I make a set? (x = set())
2. How do I make a function (def name(): pass) or (lambda x: x)
3. How do I make an object? (class Name: init and stuff)
4. How do I instantiate an object? (name = Name())
5. How do I quit python from the prompt? type "exit()"

## Git Software
1. How do I connect? 
	- [https://docs.gitlab.com/ee/ssh/#generating-a-new-ssh-key-pair]
	- Open up Git bash
	- Paste in: ssh-keygen -t ed25519 -C "email@example.com"
	- Enter twice to install in the default place with no password
	- Go to [https://gitlab.com/]
	- Settings > SSH Keys > Add New Key
	- cat ~/.ssh/id_ed25519.pub | clip
	- Paste it in and hit "add key"
2. How do I know what repository I'm in? (git status)
3. How do I start a repository? (git init)
4. How do I take someone else's repository? (git clone git@gitlab.com:Trijeet/jerks.git)
5. How do I track changes? (git add x)
6. How do I add something to the record? (git commit -m "this is what I'm adding")
7. How do I store my changes online? (git push -u origin branch_name)
8. How do I get other peoples' changes? (git pull)