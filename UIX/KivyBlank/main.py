##Kivy:
from kivy.app import App
from kivy.base import runTouchApp
from kivy.lang import Builder
from kivy.properties import ListProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.settings import SettingsWithSidebar

from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition

##StdLib
import time
import random

#Local:
from settingsjson import settings_json

class FirstScreen(Screen):
    pass

class SecondScreen(Screen):
    pass

class ColourScreen(Screen):
    colour = ListProperty([1., 0., 0., 1.])

class Interface(Screen):
    pass

class MyScreenManager(ScreenManager):
    def new_colour_screen(self):
        name = str(time.time())
        s = ColourScreen(name=name,
                         colour=[random.random() for _ in range(3)] + [1])
        self.add_widget(s)
        self.current = name

class RNGApp(App):
    def build(self):
        self.root = Builder.load_file("./rng.kv")
        self.settings_cls = SettingsWithSidebar
        self.use_kivy_settings = False
        setting = self.config.get('example', 'boolexample')
        Window = MyScreenManager()
        Window.add_widget(FirstScreen(name='first'))
        Window.add_widget(SecondScreen(name='second'))
        Window.add_widget(ColourScreen(name='ColourScreen'))
        Window.add_widget(Interface(name='settings'))
        return Window

    def build_config(self, config):
        config.setdefaults('example', {
            'boolexample': True,
            'numericexample': 10,
            'optionsexample': 'option2',
            'stringexample': 'some_string',
            'pathexample': '/some/path'})

    def build_settings(self, settings):
        settings.add_json_panel('Panel Name', self.config, data=settings_json)

    def on_config_change(self, config, section, key, value):
        print(config, section, key, value)

RNGApp().run()