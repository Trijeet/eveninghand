
from bs4 import BeautifulSoup
import requests as rq
import shutil

r = rq.get("https://www.allrecipes.com/recipe/171660/kaiserschmarren/")
print(r.status_code)

soup = BeautifulSoup(r.text, 'html.parser')

title = soup.find_all("h1", {'class': "recipe-summary__h1"})[0].text
print(title)

htmlIngredients = soup.find_all('span', {'class': "recipe-ingred_txt added"})
print(htmlIngredients)
# for i in htmlIngredients:
# 	print(i.text)

# htmlDirections = soup.find_all('span', {'class': "recipe-directions__list--item"})
# print(htmlDirections)

# htmlRating = soup.find_all('span', {'itemprop': 'aggregateRating'})
# print(htmlRating)


# for i in range(171660, 171665):
#     r = rq.get(f"https://www.allrecipes.com/recipe/{i}/")
#     print(r.status_code)
    # if r.status_code==200:
    #     do x
#     soup = BeautifulSoup(r.text, 'html.parser')
#     title = soup.find_all("h1", {'class': "recipe-summary__h1"})[0].text
#     print(title)