import socket 

def makeClient(host, port):
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM) 
    s.connect((host,port))
    return s

host = '127.0.0.1' 
port = 30303


my_client = makeClient(host, port)

message = "50"

# message sent to server 
my_client.send(message.encode('ascii')) 