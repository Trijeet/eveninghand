# Problem Set 1

### Sets
1. 

2. 

### String manipulations:
1. Define a function that, given an arbitrary string, will print and return the count the number of vowels in the string to std out.

2. Define a function that, given an arbitrary string, will print and return the count the number of consonants in the string to std out.

3. Define a function that, given two arbitrary strings (A and B), will print and return the number of times string "A" occurs in string "B".


### Keep it Classy:
1. Define an Object called Student who has a name, age, and favorite color
2. Teach the Student how to read.
3. Teach the Student