# prompt for number of circles, n, and a radius, r
# draw n concentric filled circles about the origin
# decreasing radii (max radius being r)

import turtle
import time
import math
import random

turtle.colormode(1.0)

print("This program draws concentric circles of many different colors\n")

# prompt for the input
while True:
    fig_num = input("Enter the number of circles to draw: ")
    if fig_num.isdigit():
        fig_num = int(fig_num)
        if 1 <= fig_num:
            break
    print("The number must be an integer and at least 1.")
    print("Please try again.\n")

while True:
    radius = input("\nEnter the radius (>=50, <=200) of the largest circle: ")
    if radius.isdigit():
        radius = float(radius)
        if 50 <= radius <= 200:
            break
    print("The radius must be an integer between 50 and 300.")
    print("Please try again.")
            

fig_num = int(fig_num)
decrement = radius/fig_num

# draw fig_num filled concentric circles, randomly changing colors
for j in range(fig_num):
    turtle.up()
    turtle.goto(0,-radius)
    
    (r,g,b) = (random.random(), random.random(), random.random())
    turtle.pencolor(r,g,b)
    turtle.fillcolor(r,g,b)
    turtle.down()
    turtle.begin_fill()
    turtle.circle(radius)
    turtle.end_fill()
    
    radius -= decrement
    
turtle.hideturtle()






        
