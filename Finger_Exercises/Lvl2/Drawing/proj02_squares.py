# prompt for a positive integer, n, drawing
# n evenly spaced, filled squares around the origin

import turtle
import time
import math
import random

turtle.colormode(1.0)
turtle.down()

print("This program draws squares of many colors.\n")

# prompt for the number of squares to draw
while True:
    fig_num = input("Enter the number of squares to draw: ")
    if fig_num.isdigit():
        fig_num = int(fig_num)
        if 1 <= fig_num:
            break
    print("The number must be an integer and at least 1.")
    print("Please try again.\n")

fig_num = int(fig_num)
increment = 360/fig_num

# draw fig_num filled squares, randomly changing colors
for j in range(fig_num): 
    (r,g,b) = (random.random(), random.random(), random.random())
    turtle.pencolor(r,g,b)
    turtle.fillcolor(r,g,b)

    turtle.begin_fill()
    for i in range(4):
        turtle.forward(100)
        turtle.right(90)
    turtle.end_fill()
    
    turtle.right(increment)
    
turtle.hideturtle()






        
