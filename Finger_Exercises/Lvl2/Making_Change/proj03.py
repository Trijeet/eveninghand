#!/usr/bin/python3

WELCOME = "Welcome to the vending machine change maker program"
INIT_MSG = "Change maker initialized."
CHANGE_MSG = "\nPlease take the change below."
REFUND_MSG = "See store manager for remaining refund."
OUT_MSG = "Machine is out of change."
DUE_MSG = "Amount due is:"
STOCK_MSG = "Stock contains:"
PRICE_MSG = "\nEnter the purchase price (xx.xx) or `q' to quit: "

MENU = """
Menu for deposits:
  'n' - deposit a nickel
  'd' - deposit a dime
  'q' - deposit a quarter
  'o' - deposit a one dollar bill
  'f' - deposit a five dollar bill
  'c' - cancel the purchase
"""

# read in the starting counts of the coin and bills
print(WELCOME)
nickels = dimes = quarters = 25
ones = fives = 0

print(INIT_MSG)
print(STOCK_MSG)
print("  ", nickels, "nickels")
print("  ", dimes, "dimes")
print("  ", quarters, "quarters")
print("  ", ones, "ones")
print("  ", fives, "fives")

price_response = input(PRICE_MSG)
while price_response != 'q':

    # convert to number of cents and check that is multiple of 5
    price = int(round(float(price_response) * 100)) 
    if (price % 5) or price < 0:
        print("Illegal price: Must be a non-negative multiple of 5 cents.")
        price_response = input(PRICE_MSG)
        continue

    # gather payment
    print(MENU)
    payment = 0
    denomination = ""   # arbitrary value, not equal to 'c'
    while (payment < price) and (denomination != 'c'):
        due = price-payment
        print("Payment due: ", end="")
        if due > 100:
            print(due//100, "dollars and ", end="")
        print(due%100, "cents")
        
        denomination = input("Indicate your deposit: ")
        if denomination == 'n':
            payment += 5
            nickels +=1
        elif denomination == 'd':
            payment += 10
            dimes += 1
        elif denomination == 'q':
            payment += 25
            quarters += 1
        elif denomination == 'o':
            payment += 100
            ones += 1
        elif denomination == 'f':
            payment += 500
            fives += 1
        elif denomination != 'c':
            print("Illegal selection:", denomination)

##    # for testing/debugging
##    print ("Price is", price)
##    print ("Payment is", payment)
##    print ("Return requested:", end=' ')
##    print (denomination == 'c')

    # determine change to be returned
    if denomination == 'c':
        change = payment
    else:
        change = payment - price

##    # for testing/debugging
##    print ("Change needed is", change)

    print(CHANGE_MSG)
    if change == 0:
        print("  No change due.")
    else:
        # figure out how many coins of each denomination to return
        q2return = change // 25
        if q2return > quarters:
            q2return = quarters
        change -= 25 * q2return
        quarters -= q2return

        d2return = change // 10
        if d2return > dimes:
            d2return = dimes
        change -= 10 * d2return
        dimes -= d2return

        n2return = change // 5
        if n2return > nickels:
            n2return = nickels
        change -= 5 * n2return
        nickels -= n2return

        #print the change
        if q2return > 0:
            print("  ", q2return, "quarters")
        if d2return > 0:
            print("  ", d2return, "dimes")
        if n2return > 0:
            print("  ", n2return, "nickels")

        if change > 0:
            print(OUT_MSG)
            print(REFUND_MSG)
            print(DUE_MSG, end=" ")
            if change > 100:
                print(change//100, "dollars and ", end="")
            print(change%100, "cents")

    print()
    print(STOCK_MSG)
    print("  ", nickels, "nickels")
    print("  ", dimes, "dimes")
    print("  ", quarters, "quarters")
    print("  ", ones, "ones")
    print("  ", fives, "fives")
        
    price_response = input(PRICE_MSG)

stock_total = 500*fives + 100*ones + 25*quarters + 10*dimes + 5*nickels
print()
print("Total: ", end="")
if stock_total>100:
    print(stock_total//100, "dollars and ", end="")
print(stock_total%100, "cents")


