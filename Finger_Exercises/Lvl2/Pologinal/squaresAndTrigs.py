# every square num is the sum of two triagonal nums
# This is easy. The sqrt tells you which triagonal numbers you need
# sqrt of 36 is 6. You need the 6th and 5th triagonal numbers
# triag(n) = ((n*n)+n)/2
# wfp, 9/4

import math
num = int(raw_input("Num:"))

sqrt = math.sqrt(num)

if sqrt != int(sqrt):
    print num," is not a square number"
else:
    temp = sqrt
    trig1 = (temp*temp+temp)/2
    temp = temp - 1
    trig2 = (temp*temp+temp)/2
    print num,"is a square number with a root of",int(sqrt)
    print "It is the sum of the triagonal numbers",int(trig1),"and",int(trig2)
    
