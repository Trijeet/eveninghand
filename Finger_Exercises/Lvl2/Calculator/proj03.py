# simple calculator
# wfp, 9/06

keepGoing=True

while keepGoing:
    expression = raw_input("Give me an expression: ")
    numStr1,op,numStr2 = expression.split()
    num1 = float (numStr1)
    num2 = float (numStr2)

    if op == '+':
        result = num1 + num2
    elif op == '-':
        result = num1 - num2
    elif op == '*':
        result = num1 * num2
    elif op == '/':
        if num2 == 0:
            print "can't divide by 0, try again"
            continue
        else:
            result = num1 / num2
    
    print numStr1,op,numStr2,'equals',result
    tryAgain = raw_input("Want to try again:")
    if tryAgain not in 'yesYesYES':
        keepGoing=False

print "Thanks for playing"
raw_input("Hit any key to continue")
