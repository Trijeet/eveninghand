# Translates user input into "Gibberish".
# User is prompted for the primary and secondary Gibberish syllables, as well as
#   a sentence to translate.
#
# Rules:
#   Adds a gibberish syllable to the first consecutive group of vowels in a word
#   Adds a secondary syllable to each additional vowel group in a word


import string

print("English to Gibberish translator")
user = ""
vowels = "aeiouAEIOU"

while user.lower() != "n":
    cons1 = input("Enter your first Gibberish syllable (add * for the vowel substitute): ")
    check = True
    while check:
        for letter in cons1:
            if letter not in string.ascii_letters and letter != "*":
                cons1 = input("Syllable must only contain letters or a wildcard ('*'): ")
                break
        else:
            check = False
            
    cons2 = input("Enter the second Gibberish syllable (* for vowel substitute): ")
    while check:
        for letter in cons2:
            if letter not in string.ascii_letters and letter != "*":
                cons2 = input("Syllable must only contain letters or a wildcard ('*'): ")
                break
        else:
            check = False
            
    sentence = input("Please enter a sentence you want to translate:\n--> ")
    prev_vowel = False
    same_word = False
    new_sentence = ""
    for letter in sentence:
        if letter in vowels and not prev_vowel:
            if not same_word:
                for C in cons1:
                    if C == "*":
                        new_sentence += letter
                    else:
                        new_sentence += C
                same_word = True
            else:
                for C in cons2:
                    if C == "*":
                        new_sentence += letter
                    else:
                        new_sentence += C
            new_sentence += letter
            prev_vowel = True
        else:
            new_sentence += letter
            prev_vowel = False
            if letter == " ":
                same_word = False
        
    print("Your final sentence:")
    print(new_sentence)
    print()

    user = input("Play again? (y/n) ")
    while user != "y" and user != "n":
        user = input("Please enter y to continue or n to quit: ")
