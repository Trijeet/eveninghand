first = input("first syllable: ")
second = input("second syllable: ")
word = input("word: ")
vowels = "aeiouAEIOU"
new_word = ""
previous_letter = ""
done_with_first_vowel = False

while True:
    for ch in word:
        if ch in vowels and previous_letter not in vowels:
            if not done_with_first_vowel:
                if first[0]=='*':
                    new_word += ch + first[1:]
                else:
                    new_word += first
                done_with_first_vowel = True
            else:
                if second[0]=='*':
                    new_word += ch + second[1:]
                else:
                    new_word += second
        new_word += ch
        previous_letter = ch

    print(new_word)
    reply = input("Another? ")
    if reply in "noyesNOYES":
        break
        
