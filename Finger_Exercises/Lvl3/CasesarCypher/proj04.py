alpha = 'abcdefghijklmnopqrstuvwxyz'

response = raw_input("q for quit, d for decode, e for encode:")
while response != 'q':
    if response == 'e':
        string = raw_input("Give me a string to encode:")
        rotate = int(raw_input("Give me a rotation:"))
        newAlpha = alpha[rotate:]+alpha[:rotate]
        # print newAlpha
        output = ''
        for l in string:
            if l in alpha:
                indx = alpha.index(l)
                output += newAlpha[indx]
            else:
                output += l
        print 'Encoded string is:',output
    elif response == 'd':
        string = raw_input("Give me a string to decode:")
        target = raw_input("Give me a word in the string:")
        for rotate in range(1,27):
            output = ''
            newAlpha = alpha[rotate:]+alpha[:rotate]
            for l in string:
                if l in newAlpha:
                    indx = newAlpha.index(l)
                    output += alpha[indx]
                else:
                    output += l
            # print output
            if target in output:
                print 'The rotation was:',rotate
                print 'The decoded string is:',output
                break
        else:
            print "Couldn't find a decoding"
    else:
        print 'Bad command, try again'
    response = raw_input("q for quit, d for decode, e for encode:")

print "Thanks for playing"
