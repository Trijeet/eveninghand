import turtle

#Introduction to the Project
print "This is an implementation of the popular word game - Hangman"
print "You will enter a word or phrase to be guessed"
print "The player then has to guess one letter at a time until he/she has guessed",
print "all of the letters in the phrase or has run out of chances."
print "The player has to guess the phrase using at most 6 chances.\n"


# incharStr contains the letters/digits correctly guessed by the player
incharStr = ""
# outcharStr contains the letters/digits incorrectly guessed by the player
outcharStr = ""
remChances = 6
# Read the movie name or actor name from the file

print "##############################"
print "Starting a new game of Hangman"
print "##############################\n"

movieOrActor = raw_input("please enter phrase to guess: ")
gameWord = ""
for c in movieOrActor:
    if c!=' ' and c!='-' and c!="'":
        gameWord+="_ "
    else:
        gameWord+=c+" "

while remChances>0:  
    print "Chances Remaining   : ", remChances
    print "Missed Letters/Digits : ",
    if not len(outcharStr):
        print "None"
    else:
        print outcharStr
    print "\n",gameWord

    if gameWord.find('_')==-1: # Check if all the letters/digits have been guessed
        print "Well Done! You have guessed the phrase correctly."
        break

    while True:
        # Prompt the user for a guess
        userInput = raw_input("\nYour guess (letters only): ")

        # only characters are allowed and check if the letter/digit has already been guessed.
        if not userInput.isalpha():
             print "Not a valid character. Please enter a letter ."
        elif userInput.lower() in outcharStr or userInput.lower() in incharStr:
            print "You have already tried this letter or digit. Guess again!\n"
        else:
            break
        
    #Check if the letter/digit is present in the name to be guessed.
    #If yes, concat it to the incharStr.     
    if userInput.lower() not in movieOrActor.lower():
        outcharStr += userInput.lower()
        print "This character is not present in the name."
        remChances -= 1
    else:
        incharStr += userInput.lower()
        index = movieOrActor.lower().find(userInput.lower())
        while index!=-1:
            gameWord = gameWord[0:index*2]+movieOrActor[index]+' '+gameWord[index*2+2:]
            index = movieOrActor.lower().find(userInput.lower(),index+1)

else:
    print "You have lost the game."
    print "\nThe phrase was: "
    for c in movieOrActor:
        print c,
    print



   


