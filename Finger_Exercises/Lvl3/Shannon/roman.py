# Add two Roman Numerials
# Only handles sums to about 380

# Algorithm
#    1. Read Roman Number as a string and convert number to decimal
#       Iterate through number_string
#          if subtraction indicated by pattern, e.g. IX, subtract
#            else add
#    2. Read and convert second number
#    3. Find sum of the two numbers (int)
#    4. Print sum as Roman Number
#       a. Convert sum to a string
#       b. Iterate through sum
#          i. find place, e.g. 100's, 10's, 1's (name: mag)
#          ii. based on place and digit, print appropriate value
#              (lots of special cases)

C = 100
L = 50
X = 10
V = 5
I = 1

r = raw_input("Enter First Roman Number (no spaces): ")

total = 0
for i,ch in enumerate(r):
    if ch == "C":
            total += C
    elif ch == "L":
        if i < len(r)-1 and r[i:i+2] == "LC": # check for last digit and subtraction pattern
            total -= L
        else:
            total += L
    elif ch == "X":
        if i < len(r)-1 and (r[i:i+2] == "XC" or r[i:i+2] == "XL"):
            total -= X
        else:
            total += X
    elif ch == "V":
        if i < len(r)-1 and (r[i:i+2] == "VL" or r[i:i+2] == "VX"):
            total -= V
        else:
            total += V
    elif ch == "I":
        if i < len(r)-1 and (r[i:i+2] == "IX" or r[i:i+2] == "IV"):
            total -= I
        else:
            total += I
    else:
        print "Error: ",ch
            
print "Value of", r, ": ",total
t2 = total

r = raw_input("Enter Second Roman Number (no spaces): ")

total = 0
for i,ch in enumerate(r):
    if ch == "C":
            total += C
    elif ch == "L":
        if i < len(r)-1 and r[i:i+2] == "LC":
            total -= L
        else:
            total += L
    elif ch == "X":
        if i < len(r)-1 and (r[i:i+2] == "XC" or r[i:i+2] == "XL"):
            total -= X
        else:
            total += X
    elif ch == "V":
        if i < len(r)-1 and (r[i:i+2] == "VL" or r[i:i+2] == "VX"):
            total -= V
        else:
            total += V
    elif ch == "I":
        if i < len(r)-1 and (r[i:i+2] == "IX" or r[i:i+2] == "IV"):
            total -= I
        else:
            total += I
    else:
        print "Error: ",ch
            
print "Value of", r, ": ",total

r_sum = t2 + total

print "Digital sum is: ", r_sum

print "Roman sum is: ",

#n_str = raw_input("Enter a whole number: ")  # for testing
n_str = str(r_sum)
for i,ch in enumerate(n_str):
    mag = 10**(len(n_str)-1-i)  # find place, e.g. 100's, 10's, 1's
    if mag == 100:
        digit = "C"
    elif mag == 10:
        digit = "X"
    elif mag == 1:
        digit = "I"
    else:
        digit = "0"
        print "Error(i,ch,mag): ", i, ch, mag
    # handle individual cases
    if ch == '9':
        if digit == "X":
            print "X C",
        elif digit == "I":
            print "I X",
    elif ch == '4':
        if digit == "X":
            print "X L",
        elif digit == "I":
            print "I V",
    elif ch == '5':
        if digit == "X":
            print "L",
        elif digit == "I":
            print "V",
    elif ch == '6':
        if digit == "X":
            print "L X",
        elif digit == "I":
            print "V I",
    elif ch == '7':
        if digit == "X":
            print "L X X",
        elif digit == "I":
            print "V I I",
    elif ch == '8':
        if digit == "X":
            print "L X X X",
        elif digit == "I":
            print "V I I I",
    else:
        for i in range(int(ch)):
            print digit,
    
