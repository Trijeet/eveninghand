#!/usr/bin/env python3

import flask

# - New Stuff - 
import dill
import folium
import geopy
from geopy.extra.rate_limiter import RateLimiter

app = flask.Flask(__name__)

@app.route('/')
def index():
    fields = ["street", "city", "county", "state", "country", "postalcode"]
    geo_params = {k: (flask.request.args.get(k) or "") for k in fields}


    geolocator = geopy.geocoders.Nominatim(user_agent="evening_hand")
    geocode = RateLimiter(geolocator.geocode, min_delay_seconds=1)
    this_location = geocode(geo_params)

    return dill.dumps(this_location, protocol=2), 200

@app.route('/map')
def map():
    '''
    Generates a map using set of coordinates provided.

    Centers the map around the first pair of coordinates.  
    '''

    set_of_coords = flask.request.args.get("points")
    descriptions = flask.request.args.get("tooltips")
    try:
        set_of_coords = [dill.loads(i) for i in set_of_coords]
    except:
        pass
    if not set_of_coords:
        set_of_coords = [(35.9640421, -78.9491965), (35.9661000, -78.9512000)]
        descriptions = ["T's Cave", "Random place"]

    folium_map = folium.Map(location=set_of_coords[0], zoom_start=14)
    for point, detail in zip(set_of_coords, descriptions):
        folium.Marker(point, popup=f"<i>{detail}</i>", tooltip="Click to see name!").add_to(folium_map)
    return folium_map._repr_html_()


if __name__ == '__main__':
    app.run(debug=True)