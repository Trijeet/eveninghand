from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField
from wtforms.validators import DataRequired, NumberRange


class RollForm(FlaskForm):
    username = StringField('Username')
    number_of_rolls = IntegerField(
        'Number of Rolls', validators=[DataRequired(), NumberRange(min=1, max=1000)])
    max_roll = IntegerField('Max Roll',
                            validators=[DataRequired(), NumberRange(min=1, max=1000)])
    submit = SubmitField('Roll')
