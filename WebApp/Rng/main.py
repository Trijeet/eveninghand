import flask


app = flask.Flask(__name__)


@app.route("/")
def landing():
    return flask.render_template("landing.html", x=5, cool_kids=["Elio", "Jade", "Katie", "T"])


@app.route("/feelinggood")
def feelingGood():
    return flask.render_template("feelinggood.html", x=10,
                                 cool_kids=["Elio", "Jade", "Katie"])


if __name__ == "__main__":
    app.run(debug=True)
