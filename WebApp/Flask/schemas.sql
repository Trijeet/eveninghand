CREATE TABLE IF NOT EXISTS rollapp.rolls (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(30),
    max_roll INT NOT NULL CHECK (max_roll>0),
    number_of_rolls INT NOT NULL
);