import flask
from flask import g
import mariadb
import os

app = flask.Flask(__name__)

app.config.update(
    FLASK_APP="app.py",
    TESTING=True,
    SECRET_KEY=b'_5#y2L"F4Q8z\n\xec]/',
)

config = {
    'host': "localhost",
    'port': 3306,
    'user': "root",
    'password': "password",
    'database': 'rollapp',
    'ssl': 1
}

@app.teardown_appcontext
def closeConnection(exception):
    db = getattr(g, "_database", None)
    if db is not None:
        db.close()


@app.route("/")
def landing():
    return flask.render_template("landing.html", x=5000, cool_kids=["Elio", "Jade", "Katie", "T"])


@app.route("/history")
def history():
    connection = mariadb.connect(**config)
    cursor = connection.cursor()

    sql_data = []

    if flask.request.method == 'GET':
        cursor.execute("SELECT * FROM rolls ORDER BY id DESC")
        row_headers=[x[0] for x in cursor.description] 
        rv = cursor.fetchall()
        if rv:
            sql_data.append(row_headers)
            for row in rv:
                sql_data.append(dict(zip(row_headers,row)))
        else:
            flask.flash("You haven't rolled yet!")
    connection.close()
    return flask.render_template("history.html", sql_data=sql_data)


@app.route('/roll', methods=['GET','POST','PUT','DELETE'])
def roll():
    connection = mariadb.connect(**config)
    cursor = connection.cursor()

    if flask.request.method == 'GET':
        pass
   
    if flask.request.method == 'POST':
        username = flask.request.form['username']
        max_rolls = flask.request.form['max-roll']
        number_of_rolls = flask.request.form['number-of-rolls']

        cursor.execute("INSERT INTO rolls (username, max_roll, number_of_rolls) VALUES (?,?,?)",[username, max_rolls, number_of_rolls])
        connection.commit()
        json_data = { 'success': True }
        return flask.redirect("history")

    if flask.request.method == 'PUT':
        id = flask.request.form['id']
        username = flask.request.form['username']
        max_rolls = flask.request.form['max-roll']
        number_of_rolls = flask.request.form['number-of-rolls']

        cursor.execute("UPDATE rolls SET username = ?, max-roll = ?, number_of_rolls = ? WHERE id = ?", [username, max_rolls, number_of_rolls, id])
        connection.commit()
        json_data = { 'success': True }
        return flask.redirect("history")

    if flask.request.method == 'DELETE':
        id = flask.request.form('id')
        cursor.execute("DELETE FROM rolls WHERE id = ?",[id])

        connection.commit()
        json_data = { 'success': True }
    
    connection.close()
    return flask.render_template("roll.html", title="Roll!")


if __name__ == "__main__":
    app.run(debug=True)
