import mariadb

config = {
    'host': "localhost",
    'port': 3306,
    'user': "root",
    'password': "password",
    'ssl': 1
}

my_data = [("i", 6, i) for i in range(10)]

if __name__ == "__main__":
    connection = mariadb.connect(**config)
    cursor = connection.cursor()

    cursor.execute("CREATE DATABASE IF NOT EXISTS rollapp;")
    with open("./schemas.sql") as f:
        sql_statement = f.read()
        cursor.execute(sql_statement)
    
    cursor.executemany("INSERT INTO rollapp.rolls (username, max_roll, number_of_rolls) VALUES (?, ?, ?)", my_data)
    connection.commit()
    connection.close()