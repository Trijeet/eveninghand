import requests as rq
from bs4 import BeautifulSoup as bs
import pandas as pd

class Pythonscraper3(object):
    def __init__(self, *args, **kwargs):
        super(Pythonscraper3, self).__init__()
        self.pagenum = 1
        self.hl = "https://pypi.org/search/?q=&o="
        self.framework = 'https://pypi.org/search/?q=&o=&c=Framework+%3A%3A+'
        self.category_links = ['https://pypi.org/search/?q=&o=&c=Framework+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Topic+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Development+Status+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=License+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Programming+Language+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Operating+System+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Environment+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Intended+Audience+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Natural+Language+%3A%3A+',
                               'https://pypi.org/search/?q=&o=&c=Typing+%3A%3A+']
        self.page_tail= '&o=&q=&page=1'
        self.list_of_categories= ["Framework", 'Topic', 'Development_Status', 'License', 'Programming_Language', 'Operating_System', 'Environment', 'Intended_Audience', 'Natural_Language', 'Typing']
        self.list_of_checkbox = []
        self.alltheinfo = []
        self.listoflinks = []
        self.getLinks()
        self.disabledRacoon()
        self.getPlugins()
#         self.saveStuff()

    '''O'''
    def parse_url(self, url: str) -> object:
        '''Takes a string and parses it to check if it exists or not
        if it does it returns a bs object'''
        r = rq.get(url)
        if r.status_code==200:
            print('uwu')
            page = r.text
            return bs(page, "lxml")
        else:
            print('could not get text.')
           
    '''O'''
    def lookForName(self, project: object) -> str:
        '''Takes in an link to an specific plugin in the https://pypi.org/ site
        and gets the name of the plugin'''
        self.name = project.find('h1')
        return self.name.text.strip().split(' ')[0]
   
    '''O'''
    def lookForVersion(self, project: object) -> str:
        '''Takes in an link to an specific plugin in the https://pypi.org/ site
        and gets the version of the plugin'''
        self.version = project.find('h1')
        return self.version.text.strip().split(' ')[1]
   
    '''O'''    
    def lookForPip(self, project: object) -> str:
        '''Takes in an link to an specific plugin in the https://pypi.org/ site
        and gets the pip install of the plugin'''
        try:
            self.pip = project.find('p', {'class': 'package-header__pip-instructions'})
            return self.pip.text.split('\n')[1].strip()
        except:
            return "No PIP"
       
    '''O'''
    def lookForDates(self, project: object) -> str:
        '''Takes in an link to an specific plugin in the https://pypi.org/ site
        and gets all the times the plugin has been updated'''
        self.dates_as_list = self.link.findAll('p', {'class': 'release__version-date'})
        self.release_date = self.dates_as_list[-1].text.strip()
        self.dates = [date_listed.text.replace(",", ";").strip() for date_listed in self.dates_as_list]
        self.dates_as_string = ", ".join(self.dates)
        return self.dates_as_string
   
    '''O'''
    def lookForDateOfRelease(self):
        '''returns the date the plugin was added to the site from the variable
        in the lookforDates function'''
        return self.release_date
   
    '''O'''
    def lookForHome(self, project: object) -> str:
        '''Takes in an link to an specific plugin in the https://pypi.org/ site
        and gets the home page link of the author of the plugin'''
        try:
            self.home = project.find('a', {'class': "vertical-tabs__tab vertical-tabs__tab--with-icon vertical-tabs__tab--condensed"})
            return self.home["href"]
        except TypeError:
            return "No Homepage"
       
    '''O'''
    def lookForLicense(self, project: object) -> str:
        '''Takes in an link to an specific plugin in the https://pypi.org/ site
        and gets the licnese of the plugin'''
        self.license = project.findAll('p')
        for license_listed in self.license:
            if license_listed.text.split(" ")[0]=="License:":
                return license_listed.text[9:]
           
    '''O'''        
    def lookForAuthor(self, project: object) -> str:
        '''Takes in an link to an specific plugin in the https://pypi.org/ site
        and gets the author of the plugin'''
        self.author = project.findAll('p')    
        for author_listed in self.author:  
            if author_listed.text.split(" ")[0]=="Author:":
                return author_listed.text[8:]
           
    '''O(n)'''        
    def getLinks(self):
        '''Looks for all the links/checkboxes within the given accordion id and puts them in a list'''
        self.link = self.parse_url(self.hl)
        self.framework_box =self.link.findAll('div', {'id': 'accordion-Framework'})[0]
        for check_box in self.framework_box.findAll('li'):
            self.list_of_checkbox.append(check_box.text.strip().split("::"))
           
    '''O'''
    def checkUnder10000(self, url: str) -> bool:
        '''Checks if a L1 checkbox results in over 10,000 and if so returns True'''
        self.url_in_question = self.parse_url(url)
        self.ten_thousand = self.url_in_question.findAll('form', {"action":"/search/"})[2]
        return self.ten_thousand.find('strong').text.strip() == '10,000+'
   
    '''O'''
    def notReal(self, url: str) -> bool:
        '''Checks if a L1 checkbox results in 0 and if so returns True'''
        self.sham = self.parse_url(url)
        self.fake = self.sham.findAll('form', {"action":"/search/"})[2]
        return self.fake.find('strong').text.strip() == '0'
   
    '''O(n^3)'''
    def disabledRacoon(self):
        '''Cleans up the list of the checkboxes by removing the /n/n/n/n and
        exchanging any spaces for +'''
        self.allurls= []
        for sub_check_box in self.list_of_checkbox:
            for plugin in sub_check_box:
                self.this_framework = plugin.split('\n\n\n\n')
                print(self.this_framework)
                self.allurls.extend([mapache_descapacitado.replace(' ', '+') for mapache_descapacitado in self.Customs(counter = 0)])
        print('owo')
       
    '''O(n)'''
    def Customs(self, counter = 0) -> bool:
        '''Combines the first item in a list with the general framework link
        then checks if it exsits or it is over the bounds; if it is jus tover
        the bounds then it takes that link and combines it with it with the
        next item in the list for every item in the list not including the first
        otherwise, it prints out not real'''
        self.url = self.framework + str(self.this_framework[0])
        print(self.url)
        if counter > 0:
            print(self.url)
            self.url += '+%3A%3A+' + self.this_framework[counter]
        counter += 1
        self.too_much = self.checkUnder10000(self.url)
        self.does_not_exist = self.notReal(self.url)
        if self.too_much:
            self.over_limit = []
            for sub_checkbox in range(1, len(self.this_framework)):
                self.sub_url = self.url + '+%3A%3A+' + self.this_framework[sub_checkbox]
                self.over_limit.append(self.sub_url)
            print(self.over_limit, 'customs')
            return self.over_limit
        if self.does_not_exist:
            return ['not real']
        else:
            print(self.url, 'else')
            return [self.url]
       
    '''O(n+(O(n*9)^n)'''
    def getPlugins(self):
        '''Gets the plugin urls from the first page of the links gotten
        from the customs function'''
        print(self.allurls)
        for that_url in self.allurls:
            if that_url=="not+real":
                print("not real")
            else:
                self.website = self.parse_url(that_url).findAll("span", {'class': "package-snippet__name"})
                self.incomplete_url = 'https://pypi.org/project/'
                for span in self.website:
                    self.complete_url = self.incomplete_url + span.text.strip() + "/"
                    self.listoflinks.append(self.complete_url)
                    self.link = self.parse_url(self.complete_url)
                self.organizer()
               
    '''O(n*9)'''                
    def organizer(self):
        '''Gets the desired information from every link given by
        the getPlugins function'''
        for project in self.listoflinks:
            self.projectlink = self.parse_url(project)
            self.info = {}
            self.info['Link to the project'] = project
            self.info['Name'] = self.lookForName(self.projectlink)
            self.info['Version'] = self.lookForVersion(self.projectlink)
            self.info['PIP'] = self.lookForPip(self.projectlink)
            self.info['Updates'] = self.lookForDates(self.projectlink)
            self.info['Home'] = self.lookForHome(self.projectlink)
            self.info['Release'] = self.lookForDateOfRelease()
            self.info['License'] = self.lookForLicense(self.projectlink)
            self.info['Author'] = self.lookForAuthor(self.projectlink)
            self.alltheinfo.append(self.info)
           
    '''O'''    
    def saveStuff(self):
        '''Saves all the information given by the organizer function in
        an excel sheet'''
        details = pd.DataFrame.from_records(self.alltheinfo)
        details.to_csv('pythonscraper_information.csv', mode='w', index=False)